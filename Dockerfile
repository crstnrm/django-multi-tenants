# Define use image
FROM python:3.8-slim

# Keeps Python from generating .pyc files in the container
ENV PYTHONUNBUFFERED=1

# Turns off buffering for easier container logging
ENV PYTHONDONTWRITEBYTECODE=1

ENV APP_HOME=/code

# Create user app
RUN groupadd user && useradd --create-home --home-dir /home/user -g user user

# Create folder to avoid permission issues
RUN mkdir -p $APP_HOME && chown user:user $APP_HOME

# Specify work directory
WORKDIR $APP_HOME

RUN apt-get update && apt install -y \
    build-essential \
    libpq-dev \
    python3-dev \
    git

# Install dependencies
COPY ./requirements ./requirements
RUN pip install --upgrade pip \
    && pip install -r ./requirements/development.txt

# Switching to a non-root user
USER user
